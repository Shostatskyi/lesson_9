var studentsAndPoints = [
  'Алексей Петров', 0,
  'Ирина Овчинникова', 60, 
  'Глеб Стукалов', 30,
  'Антон Павлович', 30, 
  'Виктория Заровская', 30,
  'Алексей Левенец', 70,
  'Тимур Вамуш', 30,
  'Евгений Прочан', 60,
  'Александр Малов', 0
];
 
/*
  1. Создать массив students. Заполнить его данными из studentsAndPoints. Каждый элемент массива это объект в поле name имя студента а в поле point его балл. 
  Так же добавить метод show который выводит в консоль строку вида: Студент Виктория Заровская набрал 30 баллов
*/

var commonShow = function () {
	console.log('Студент %s набрал %d баллов', this.name, this.point);
}

var students = [];

studentsAndPoints.forEach(function (student, index) {
  if (isNaN(student)) {
    students.push({ name: student, point: studentsAndPoints[index+1], show: commonShow});
  }
})

students[2].show();

// 2. Добавить студентов Николай Фролов и Олег Боровой с 0 быллов.

students.push({ name: 'Николай Фролов', point: 0, show: commonShow });
students.push({ name: 'Олег Боровой', point: 0, show: commonShow });

// 3. Увеличить баллы студентам Ирина Овчинникова и Александр Малов на 30, а Николаю Фролову на 10.

function addPoint (name, point) {
  students.forEach(function (student) { // заменил map
    if(student.name === name) {
      student.point += point;
    }    
  })
}
 
addPoint('Ирина Овчинникова', 30);
addPoint('Александр Малов', 30);
addPoint('Николай Фролов', 10);


// или также через map
/*
var all_students_add = ['Ирина Овчинникова', 'Александр Малов', 'Николай Фролов'];
 
function add_point(student, index){
  if(all_students_add.find(function (student_add) {
    return student.name == student_add; 
  })) student.point +=30;
}
 
students.map(add_point);
*/

// 4. Вывести список студентов набравших 30 и более баллов без использования циклов.

console.log('\nСписок студентов, набравших 30 и более баллов: ');
students.forEach (function (student) {
  if (student.point >= 30) {
     student.show();
  }
})

// 5. Учитывая что каждая сделанная работа оценивается в 10 баллов, добавить всем студентам поле worksAmount равное кол-ву сделанных работ.

students.forEach (function (student) {
  student.worksAmount = student.point / 10;
})

// Дополнительное задание: добавить объекту students метод findByName, который принимает на вход имя студента и возвращает соответствующий объект, либо undefined.

students.findByName = function (name) {
  return students.find(function (student) {
    return student.name == name;
  })
}
 
if(students.findByName('Александр Малов')) students.findByName('Александр Малов').show();
if(students.findByName('Дмитрий Фитискин')) students.findByName('Дмитрий Фитискин').show();
